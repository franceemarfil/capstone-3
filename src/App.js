import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import NavBar from './components/NavBar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import SpecificProduct from './pages/SpecificProduct';
import Cart from './pages/Cart';
import Order from './pages/Order';
import Login from './pages/Login';
import Error from './pages/Error';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { UserProvider } from './UserContext';


export default function App() {

  // global user state
  const [user, setUser ] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
    setUser({
      id: null,
      isAdmin: null
    })
  }
  
  useEffect(() => {
    fetch(`${ process.env.REACT_APP_API_URL}/users/details`, {
      headers: { 
        Authorization: `Bearer ${ localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null 
        })
      }
    })

  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <NavBar/>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/products" component={Products}/>
          <Route exact path="/products/:productId" component={SpecificProduct}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/cart" component={Cart}/>
          <Route exact path="/order" component={Order}/>
          <Route exact path="/register" component={Register}/>
          <Route component={Error}/>
        </Switch>
      </Router>
    </UserProvider>
  );
}
