import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	const { productsData, fetchData } = props

	const [name, setName]  = useState('');
	const [description, setDescription]  = useState('');
	const [price, setPrice]  = useState('');
	const [size, setSize] = useState("")
	const [PCD, setPCD] = useState("")
	const [offset, setOffset] = useState("")
	const [color, setColor] = useState("")	
	const [quantity, setQuantity] = useState("")	

	const [products, setProducts] = useState([]);
	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);

	const [productId, setProductId] = useState("")


	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)



	const openEdit = (productId) => {
		
		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setSize(data.size)
			setPCD(data.PCD)
			setOffset(data.offset)
			setColor(data.color)
			setQuantity(data.quantity)
		})
		setShowEdit(true)
	}

	// Clears the value in the modal
	const closeEdit = () => {
		setShowEdit(false)
		setName("")
		setDescription("")
		setPrice(0)
		setSize("")
		setPCD("")
		setOffset("")
		setColor("")
		setQuantity("")
	}


	useEffect(() => {
		// console.log(props)
		const productsArr = productsData.map(product => {
			return(
				<tr key={product._id}>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>{product.size}</td>
					<td>{product.PCD}</td>
					<td>{product.offset}</td>
					<td>{product.color}</td>
					<td>{product.quantity}</td>

					<td>
						{product.isActive
							? <span>Available</span>
							: <span>Unavailable</span>
						}
					</td>

					<td>
						<Button variant="warning" size="sm" onClick={() => openEdit(product._id)}>Update</Button>
						{product.isActive
							?
							<Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
							:
							<Button variant="success" size="sm" onClick={() => unarchiveToggle(product._id, product.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
		})

		setProducts(productsArr)
	}, [productsData])


	const addProduct = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				size: size,
				PCD: PCD,
				offset: offset,
				color:color,
				quantity:quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title: "Product successfully added",
					icon: "success",
					text: "Updated",
					confirmButtonColor: '#FFB138'
				})
				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)
				setSize(data.size)
				setPCD(data.PCD)
				setOffset(data.offset)
				setColor(data.color)
				setQuantity(data.quantity)
				closeAdd()
			}else{
				fetchData()
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something went wrong.",
					confirmButtonColor: '#FFB138'
				})
				closeAdd()
			}
		})
	}


	const editProduct = (e, productId) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				size: size,
				PCD: PCD,
				offset: offset,
				color:color,
				quantity:quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data === true){
				fetchData()
				console.log(fetchData)
				Swal.fire({
					title: "Product successfully updated",
					icon: "success",
					text: "Updated",
					confirmButtonColor: '#FFB138'
				})
				closeEdit()
			}else{
				fetchData()
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again.",
					confirmButtonColor: '#FFB138'
				})
			}
		})
	}

	const archiveToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/deactivate`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			fetchData()
			// console.log(data)
			if(data === true){
				fetchData()
				Swal.fire({
					title: "Deactivated",
					icon: "success",
					text: "Successfully deactivated",
					confirmButtonColor: '#FFB138'
				})
			}

			// else{
			// 	fetchData()
			// 	Swal.fire({
			// 		title: "Deactivated",
			// 		icon: "success",
			// 		text: "Successfully deactivated",
			// 		confirmButtonColor: '#FFB138'
			// 	})
			// }
		})
	}

	const unarchiveToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			fetchData()
			// console.log(data)
			if(data === true){
				fetchData()
				Swal.fire({
					title: "Activated",
					icon: "success",
					text: "Successfully updated",
					confirmButtonColor: '#FFB138'
				})
			}

			// else{
			// 	fetchData()
			// 	Swal.fire({
			// 		title: "Success",
			// 		icon: "success",
			// 		text: "Successfully activated",
			// 		confirmButtonColor: '#FFB138'
			// 	})
			// }
		})
	}

	return(
		<>
			<div className="text-center my-4 font">
				<h2>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button variant="warning" onClick={openAdd}>Add New Product</Button>
				</div>
			</div>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white font-products">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Size</th>
						<th>PCD</th>
						<th>Offset</th>
						<th>Color</th>
						<th>Quantity</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>

		{/*EDIT MODAL*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
		        <Modal.Header closeButton>
		        	<Modal.Title>Edit Product</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			        		<Form.Group controlId="productName">
			        			<Form.Label>Name</Form.Label>
			        			<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productDescription">
			        			<Form.Label>Description</Form.Label>
			        			<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productPrice">
			        			<Form.Label>Price</Form.Label>
			        			<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productSize">
			        			<Form.Label>Size</Form.Label>
			        			<Form.Control type="text" value={size} onChange={e => setSize(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productPCD">
			        			<Form.Label>PCD</Form.Label>
			        			<Form.Control type="text" value={PCD} onChange={e => setPCD(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productOffset">
			        			<Form.Label>Offset</Form.Label>
			        			<Form.Control type="text" value={offset} onChange={e => setOffset(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productColor">
			        			<Form.Label>Color</Form.Label>
			        			<Form.Control type="text" value={color} onChange={e => setColor(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productQuantity">
			        			<Form.Label>Quantity</Form.Label>
			        			<Form.Control type="text" value={quantity} onChange={e => setQuantity(e.target.value)} required/>
			        		</Form.Group>
			        </Modal.Body>
			        <Modal.Footer>
			          <Button variant="dark" onClick={closeEdit}>
			            Close
			          </Button>
			          <Button variant="warning" type="submit">
			            Submit
			          </Button>
			        </Modal.Footer>
		        </Form>
			</Modal>

		{/* ADD MODAL */}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
		        <Modal.Header closeButton>
		        	<Modal.Title>Add Product</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			        		<Form.Group controlId="productName">
			        			<Form.Label>Name</Form.Label>
			        			<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productDescription">
			        			<Form.Label>Description</Form.Label>
			        			<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="cproductPrice">
			        			<Form.Label>Price</Form.Label>
			        			<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productSize">
			        			<Form.Label>Size</Form.Label>
			        			<Form.Control type="text" value={size} onChange={e => setSize(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productPCD">
			        			<Form.Label>PCD</Form.Label>
			        			<Form.Control type="text" value={PCD} onChange={e => setPCD(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productOffset">
			        			<Form.Label>Offset</Form.Label>
			        			<Form.Control type="text" value={offset} onChange={e => setOffset(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productColor">
			        			<Form.Label>Color</Form.Label>
			        			<Form.Control type="text" value={color} onChange={e => setColor(e.target.value)} required/>
			        		</Form.Group>
			        		<Form.Group controlId="productQuantity">
			        			<Form.Label>Quantity</Form.Label>
			        			<Form.Control type="text" value={quantity} onChange={e => setQuantity(e.target.value)} required/>
			        		</Form.Group>
			        </Modal.Body>
			        <Modal.Footer>
			          <Button variant="dark" onClick={closeAdd}>
			            Close
			          </Button>
			          <Button variant="warning" type="submit">
			            Submit
			          </Button>
			        </Modal.Footer>
		        </Form>
			</Modal>
		</>
	)
}