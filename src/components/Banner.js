import { Row, Col, Jumbotron, Container, Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({bannerProps}){
	
	return(

							<Carousel className="bottom-margin">
						  	<Carousel.Item interval={1000}>
						    <img
						      className="d-block w-100"
						      src="https://i.ibb.co/qg7SG9K/1.jpg"
						      alt="First slide"
						      position="relative"
						    />
	
						  </Carousel.Item>
						  <Carousel.Item interval={1000}>
						    <img
						      className="d-block w-100"
						      src="https://i.ibb.co/LJSbrrn/2.jpg"
						      alt="Second slide"
						      position="relative"
						    />
						    
						  </Carousel.Item>
						  <Carousel.Item interval={1000}>
						    <img
						      className="d-block w-100"
						      src="https://i.ibb.co/Hq8pL95/3.jpg"
						      alt="Third slide"
						      position="relative"
						    />
						    
						  </Carousel.Item>
						  <Carousel.Item interval={1000}>
						    <img
						      className="d-block w-100"
						      src="https://i.ibb.co/5Kp49Kq/4.jpg"
						      alt="Fourth slide"
						      position="relative"
						    />
						    
						  </Carousel.Item>
						</Carousel>

	)
}