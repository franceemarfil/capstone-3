import { Row, Col, Jumbotron, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Check({checkProps}){

	const { title, content, destination, label } = checkProps;
	
	return(
		<Jumbotron>
			<Row>
				<Col>
				</Col>
				<Col>
						<h1 className="App white-font font">{title}</h1>
						<p className="App">{content}</p>
						<div className="App">
						<Link className="btn btn-warning font-products" to={destination}>{label}</Link>
						</div>
				</Col>
				<Col>
				</Col>
			</Row>
		</Jumbotron>
	)
}