import { Row, Col, Container,  Navbar, NavbarBrand } from 'react-bootstrap';

export default function Footer() {

    return(
        <footer>
            <div class="footer">
                
                <div class="container-fluid">
                    <Row>
                        <Col xs={12} md={4}>
                        </Col>
                        <Col xs={12} md={4} className="gray-color">
                            
                            <p class="footerText App">Corona Wheels Ⓒ</p>
                            <p class="copyright App">By: Francee Marfil 2021</p>
                            
                        </Col>
                        <Col xs={12} md={4}>
                        </Col>
                    </Row>
                </div>
            </div>
        </footer>
    )
}