import { Row, Col, Card, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Highlights(){
	return(
		<Row className="bottom-margin">
			<Col xs={12} md={3}>
				<Card className="card-highlight" bg="black">
					<Card.Body>
						<Link to="/products/6110d56229aca36368999362">
						<Image src="https://www.rotawheels.com.au/assets/full/ROTA_GRID_632.jpg?20200709031114" fluid />
						</Link>
						<Card.Title className="App font-products white-font">Rota IK-F</Card.Title>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={3}>
				<Card className="card-highlight" bg="black">
					<Card.Body>
						<Link to="/products/6110d63829aca36368999373">
						<Image src="https://www.rotawheels.com.au/assets/full/ROTA_FIGHTER_25.jpg?20200709030744" fluid />
						</Link>
						<Card.Title className="App font-products white-font">Rota Fighter</Card.Title>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={3}>
				<Card className="card-highlight" bg="black">
					<Card.Body>
						<Link to="/products/6110d69a29aca36368999376">
						<Image src="https://www.rotawheels.com.au/assets/full/ROTA_SVN_382.jpg?20200709031030" fluid />
						</Link>
						<Card.Title className="App font-products white-font">Rota SVN</Card.Title>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={3}>
				<Card className="card-highlight" bg="black">
					<Card.Body>
						<Link to="/products/6110d6bd29aca36368999379">
						<Image src="https://www.rotawheels.com.au/assets/full/ROTA_TORQUE_F_640.jpg?20200709031058" fluid />
						</Link>
						<Card.Title className="App font-products white-font">Rota Torque</Card.Title>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}