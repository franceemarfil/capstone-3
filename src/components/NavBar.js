import { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import UserContext from "../UserContext"


export default function NavBar(){
	const { user, unsetUser } = useContext(UserContext)
	const history = useHistory()
	// To clear localStorage and set userState is null
	const logout = () => {
		unsetUser()
		history.push('/login')
	}


	let rightNav = (!user.id) ? (
		<>
			<Nav.Link className="ml-auto" href="/register">Register</Nav.Link>
			<Nav.Link className="ml-auto" href="/login">Log In</Nav.Link>
		</>
	) : (
		<>
			<Nav.Link className="ml-auto" href="/cart">Cart</Nav.Link>
			<Nav.Link className="ml-auto" href="/order">Orders</Nav.Link>
			<Nav.Link className="ml-auto" onClick={logout}>Log Out</Nav.Link>
		</>
	)
	
	return(
		<Navbar collapseOnSelect expand="lg" className="color-nav font">
		  <Navbar.Brand to="/">
		  	<Link to="/">
		  	<img
		  	src="https://i.ibb.co/9v0bptC/131109695-3587786284635423-5625210974813664553-n.png"
		  	height="35"
		  	/>
		  	</Link>
		  </Navbar.Brand>
		  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		  <Navbar.Collapse id="responsive-navbar-nav">
		    <Nav className="mr-auto">
		    	<Nav.Link className="font" href="/">Home</Nav.Link>
		       	<Nav.Link className="font" href="/products">Products</Nav.Link>
		       	{rightNav}
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}