import { useState, useEffect } from 'react';
import { Card, Button, Container } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';

export default function Product({ productProp }){
	

	const { _id, name, description, price } = productProp;
	

	// const [ count, setCount ] = useState(0);
	// console.log(count)


	const [orderCount, setOrderCount] = useState(30);


	const [isDisabled, setIsDisabled] = useState(false);

	// const order = () => {
	// 	setCount(count + 1);
	// 	setOrderCount(orderCount - 1);
	// };

	useEffect(() => {

		if(orderCount === 0){
			setIsDisabled(true);
		}

	}, [orderCount])

	// console.log(_id)

	return(
		<Container>
			<Card className="text-center">
				<Card.Header className="font bg-dark text-white">{name}</Card.Header>
				<Card.Body className="bg-light">
					<Card.Text>
						<h5 className="font-products">Description:</h5>

						<p className="font-products">{description}</p>

						<h5 className="font-products">Price: </h5>

						<p className="font-products">PHP {price.toFixed(2)}</p>
					</Card.Text>

					<Link className="btn btn-warning" to={`/products/${_id}`}>Details</Link>
					
				</Card.Body>
			</Card>
		</Container>
	)

}
