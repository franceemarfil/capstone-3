import { useState, useEffect } from 'react';
import Product from './Product';

export default function UserView({productsData}){

	const [ products, setProducts ] = useState([])

	useEffect(() => {

		const productsArr = productsData.map(product => {
			// shows the product that is active to the admin
			if(product.isActive === true){
				return(
					<Product productProp={product} key={product._id}/>
				)
			}else{
				return null;
			}
		});
 
		setProducts(productsArr)

	}, [productsData])

	return(
		<>
			{products}
		</>
	)
}