import { useState, useEffect, useContext } from 'react';
import { Link } from "react-router-dom";
import { Container, Card, Button, Table, Jumbotron, Row, Col } from 'react-bootstrap';
import { useHistory, Redirect} from 'react-router-dom';

import UserView from "../components/UserView";
import UserContext from "../UserContext";
import Swal from 'sweetalert2';

export default function Cart() {

	const [cart, setCart] = useState([]);


	const [total, setTotal] = useState(0);

	const [id, setId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	// const [qty, setQty] = useState(1)
	const [price, setPrice] = useState(0)


	const [productId, setProductId] = useState("")
	const [value, setValue] = useState("")

	const [ quantity, setQuantity ] = useState("")

	const { user, unsetUser } = useContext(UserContext)

	const history = useHistory();

	// LocalStorage
	const cartArr = cart.map(product => {
		console.log(product)
		return(
			<tr key={product._id}>
				<td>{product.name}</td>
				<td>{product.price.toFixed(2)}</td>
				<td>
					<div className="qty mt-2 mb-1 input-group">
						<Button variant="outline-dark" type="button" onClick={() => qtyInput(product.productId,product.quantity-1)}>-</Button>
						
						<input min="1" type="number" value={product.quantity} onChange={e => setQuantity(e.target.value)}></input>
						
						<Button variant="outline-dark" type="button" onClick={() => qtyInput(product.productId,product.quantity+1)}>+</Button>
					</div>
				</td>
				<td>{product.subtotal.toFixed(2)}</td>

				<td>
					<Button variant="outline-danger" class="row" type="button" onClick={() => removeItem(product.productId)}>Remove</Button>
				</td>
				
			</tr>
		)
	})



	//FOR GETTING YOUR LOCALSTORAGE CART ITEMS AND SETTING YOUR CART STATE:
	useEffect(()=> {
		if(localStorage.getItem('cart')){
			setCart(JSON.parse(localStorage.getItem('cart')))
	}
		console.log(cart)
	}, [])


	//FOR ADJUSTING THE QUANTITY OF AN ITEM BASED ON INPUT:
	const qtyInput = (productId, value) => {

		//use the spread operator to create a temporary copy of our cart (from the cart state)
		let tempCart = [...cart]

		//loop through our tempCart 
		for(let i = 0; i < tempCart.length; i++){
			//so that we can find the item with the quantity we want to change via its productId
			if(tempCart[i].productId === productId){
				//use parseFloat to make sure our new quantity will be parsed as a number
				tempCart[i].quantity = parseFloat(value)
				//set the new subtotal
				tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
			}
		}

		//set our cart state with the new quantities
		setCart(tempCart)

		//set our localStorage cart as well
		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}
	

	//FOR UPDATING A STATE THAT DETERMINES THE TOTAL AMOUNT:
	//whenever our cart state changes, re-calculate the total
	useEffect(()=> {
		//start with a counter initialized to zero
		let tempTotal = 0

		//loop through our cart, getting each item's subtotal and incrementing our tempTotal counter by its amount
		cart.forEach((item)=> {
			tempTotal += item.subtotal
		})

		//set our total state
		setTotal(tempTotal)
	}, [cart])


	//FOR REMOVING AN ITEM FROM CART:
	const removeItem = (productId) => {
		//use the spread operator to create a temporary copy of our cart (from the cart state)
		let tempCart = [...cart]

		//use splice to remove the item we want from our cart
		tempCart.splice([tempCart.indexOf(productId)], 1)

		//set our cart state with the new quantities
		setCart(tempCart)

		//set our localStorage cart as well
		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}


	// Check out products
	const checkOut = () => {
		let alreadyInCart = false
		//variable for the item's index in the cart array, if it already exists there
		let productIndex
		//temporary cart array
		let cart = []
		//use the spread operator to create a temporary copy of our cart (from the cart state)
		let tempCart = [...cart]

		// Checking local storage 
		if(localStorage.getItem('cart')){
			cart = JSON.parse(localStorage.getItem('cart'))
		}

		//if a product is already in our cart, just increment its quantity and adjust its subtotal
		if(alreadyInCart){
			
			cart.push({
				'productId': productId,
				'name': name,
				'price': price,
				'quantity': quantity,
				'subtotal': price * quantity
			})
			
			tempCart.splice([tempCart.indexOf(productId)], 1)

			Swal.fire({
					title: "Order placed!",
					icon: 'success',
					text: "Thank you for your purchase",
					confirmButtonColor: '#FFB138'
			})
		}

		//set our localStorage cart as well
		localStorage.setItem('cart', JSON.stringify(cart))
	}



	// For checking out of order
	const order = (productId) => {
		const checkoutCart =  cart.map((item) => {
					return {
						productId: item.productId,
						quantity: item.quantity,
						price: item.price,
					}
				})

		fetch(`${ process.env.REACT_APP_API_URL }/users/order`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				products: checkoutCart,
				totalAmount: total,
				shippingFee: 12
			 
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data === true){
				Swal.fire({
					title: "Successfully ordered!",
					icon: 'success',
					text: "You have successfully ordered this product.",
					confirmButtonColor: '#FFB138'
				})
				history.push("/products")
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again.",
					confirmButtonColor: '#C5211C'
				})
			}
		})
	}

	return(
		
			<>
			<Container>
				{(user.isAdmin === true)
                    ? <Redirect to="/products"/>
                    : 
                    
                    	cart.length === 0
                    		?

                    		<Card className="bg-dark text-white form-margin margin text-center">
                    		  <Card.Body>
                    		    <Card.Title className="form-margin font">Your cart is empty!</Card.Title>
                    		    <Button variant="warning" className="font-products" href="/products">Order now!</Button>
                    		  </Card.Body>
                    		</Card>

                    		:

                    		<Container>
                    				<div className="text-center my-4">
                    					<h2>My Shopping Cart</h2>
                    				</div>
                    				<Table striped bordered hover responsive>
                    					<thead className="bg-dark text-white">
                    						<tr>
                    							<th>Name</th>
                    							<th>Price (PHP) </th>
                    							<th>Quantity (Set of 4): </th>
                    							<th>Subtotal (PHP)</th>
                    							<th></th>
                    						</tr>
                    					</thead>
                    					<tbody>
                    						{cartArr}
                    						<tr>
                    							<td colSpan="3" >
                    								<h3>TOTAL:</h3>
                    							</td>
                    							<td colSpan="2">
                    								<h3>PHP {total.toFixed(2)}</h3>
                    							</td>
                    						</tr>
                    					</tbody>
                    				</Table>
                    				<div class="row justify-content-md-center align-items-center">
                    					<Button variant="warning" type="submit" onClick={() => order(productId)}>Check Out</Button>
                    				</div>
                    		</Container>
                    	}
            </Container>
            </>       
		

		

				
	)
}