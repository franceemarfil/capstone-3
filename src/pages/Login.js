import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Card } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(){

	const [email, setEmail]  = useState('');
	const [password, setPassword] = useState('');
	// consume the UserContext object via the useContext hook to access our global user state and its state setter
	const { user, setUser } = useContext(UserContext)
	
	const [ logInButton, setlogInButton ] = useState(false);


	function logInUser(e){
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if(typeof data.access !== "undefined"){

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
			}else{
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again.",
					confirmButtonColor: '#C5211C'
				})
			}
		})
	}

	const retrieveUserDetails = (token) => {
		fetch(`${ process.env.REACT_APP_API_URL}/users/details`, {
		    headers: { 
		      Authorization: `Bearer ${ token }`
		    }
		})
		.then(res => res.json())
		.then(data => {
		      setUser({
		        id: data._id,
		        isAdmin: data.isAdmin
		    })
		})
	}

	if(user.id != null){
		return<Redirect to="/"/>
	}

	return(
		<Container>
			<div class="row justify-content-md-center form-margin">
			<div class="col"></div>
			<div class="col">
			<Card style={{ width: '30rem' }} className=" bg-dark text-white">
				<Card.Body>
				
						
					<Form className="mt-3" onSubmit={ (e) => logInUser(e) }>
						<Form.Group className="button-margin">
							<Form.Label> Email Address: </Form.Label>
							<Form.Control type="email" placeholder="Enter your email address" value={email} onChange={e => setEmail(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Password: </Form.Label>
							<Form.Control type="password" placeholder="Enter your password" value={password} onChange={e => setPassword(e.target.value)} required/>
						</Form.Group>
						<div class="row justify-content-md-center align-items-center button-margin">
							<Button variant="warning" type="submit">Log In</Button>
						</div>
					</Form>
					
				</Card.Body>
			</Card>
			</div>
			<div class="col"></div>
			</div>
		</Container>
	)
}