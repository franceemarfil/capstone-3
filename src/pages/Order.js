import { useState, useEffect, useContext } from 'react';
import { Link } from "react-router-dom";
import { Container, Card, Button, Table } from 'react-bootstrap';

import UserView from "../components/UserView";
import UserContext from "../UserContext";
import Swal from 'sweetalert2';


export default function Order() {

const [cart, setCart] = useState([]);
const [checkoutCart] = useState([]);

useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/myOrder`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            const checkoutCart =  cart.map((data) => {
				return {
					productOrders: {
						products: {
							productId: data.productId,
							quantity: data.quantity,
							price: data.price
						}
					},
					purchasedOn: data.purchasedOn,
					shippingFee: data.shippingFee,
					totalAmount: data.total
				}
			})
            setCart(data)
            
        })
        console.log(checkoutCart)
}, [])

	    

return(
<Container>
	<Card className="bg-dark text-white form-margin">
	  <Card.Header>My Order History</Card.Header>
	  <Card.Body>
	    <blockquote className="blockquote mb-0">
	      <p>
	      	
	      </p>
	      <footer className="blockquote-footer">
	       	<h8>Thank you for your purchase</h8>
	      </footer>
	    </blockquote>
	  </Card.Body>
	</Card>
</Container>
)
}






