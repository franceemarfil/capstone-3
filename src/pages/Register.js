import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Card } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register(){

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [email, setEmail]  = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');
	// const [address, setAddresss] = useState('');
	// const [province, setProvince] = useState('');
	// const [city, setCity] = useState('');
	// const [street, setStreet] = useState('');
	// const [baranggay, setBaranggay] = useState('');
	// const [lotNumber, setLotNumber] = useState('');

	// const [ logInButton, setlogInButton ] = useState(false);
	// const [ registerButton, setRegisterButton ] = useState(false);

	const { user, setUser } = useContext(UserContext);
	const history = useHistory();

	function registerUser(e){
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				// true meaning email exists
				Swal.fire({
					title: 'Sorry',
					icon: 'error',
					text: "Email already exist!",
					confirmButtonColor: '#C5211C'
				});
			}else{
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNumber: mobileNumber,
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data === true){
						Swal.fire({
							title: 'Registration successful!',
							icon: 'success',
							text: 'Welcome to Corona Wheels!',
							confirmButtonColor: '#FFB138'
						});

						history.push('/login')
					}else{

						Swal.fire({
							title: 'Something went wrong',
							icon: 'error',
							text: 'Please check if you entered the same password or check the input details.',
							confirmButtonColor: '#C5211C'
						})
					}
				})
			}
		})
	}



	if(user.id != null){
		return <Redirect to="/"/>
	}

	return(
		<Container>
		<div class="row justify-content-md-center align-items-center form-margin">
			<div class="col"></div>
			<div class="col">
				<Card style={{ width: '30rem' }} className=" bg-dark text-white">
					<Card.Body>
						<Form className="mt-3" onSubmit={ (e) => registerUser(e) }>
							<Form.Group>
								<Form.Label>First Name:</Form.Label>
								<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Last Name:</Form.Label>
								<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Mobile Number:</Form.Label>
								<Form.Control type="text" placeholder="Enter Mobile Number" value={mobileNumber} onChange={e => setMobileNumber(e.target.value)} required/>
							</Form.Group>

							<Form.Group>
								<Form.Label> Email Address: </Form.Label>
								<Form.Control type="email" placeholder="Enter your email address" value={email} onChange={e => setEmail(e.target.value)} required/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Password</Form.Label>
								<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required/>
							</Form.Group>

							<div class="row justify-content-md-center align-items-center button-margin">
								<Button variant="warning" type="submit">Sign In</Button>
							</div>
						</Form>
					</Card.Body>
				</Card>
			</div>
			<div class="col"></div>
			</div>
		</Container>
	)
}

