import { useContext, useEffect, useState } from 'react';
import { Container, Card, Button, Form, InputGroup, FormControl } from 'react-bootstrap';
import { Link, useParams, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function SpecificProduct(){

	const { user } = useContext(UserContext)

	const { productId } = useParams();

	const history = useHistory();

	const [cart, setCart] = useState([])

	const [id, setId] = useState("")

	// qty from localStorage
	// const [qty, setQty] = useState(1)

	const [ name, setName ]= useState("")
	const [ description, setDescription ] = useState("")
	const [ price, setPrice ] = useState("")
	const [ size, setSize ] = useState("")
	const [ PCD, setPCD ] = useState("")
	const [ offset, setOffset ] = useState("")
	const [ color, setColor ] = useState("")

	// quantity - from the database 
	const [ quantity, setQuantity ] = useState(1)	

	const [value, setValue] = useState("")

		
	useEffect(() => {
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
				
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setSize(data.size)
			setPCD(data.PCD)
			setOffset(data.offset)
			setColor(data.color)
			// setQuantity(data.quantity)
		})

	}, [])

	const order = (productId) => {

		fetch(`${ process.env.REACT_APP_API_URL }/users/order`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data === true){
				Swal.fire({
					title: "Successfully ordered!",
					icon: 'success',
					text: "You have successfully ordered this product.",
					confirmButtonColor: '#FFB138'
				})
				history.push("/products")
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again.",
					confirmButtonColor: '#C5211C'
				})
			}
		})
	}

	//FOR ADJUSTING THE QUANTITY OF AN ITEM BASED ON INPUT:
	const qtyInput = (productId, value) => {
		console.log(productId)
		console.log(value)

		//use the spread operator to create a temporary copy of our cart (from the cart state)
		let tempCart = [...cart]

		//loop through our tempCart 
		for(let i = 0; i < tempCart.length; i++){
			//so that we can find the item with the quantity we want to change via its productId

			if(tempCart[i].productId === productId){
				//use parseFloat to make sure our new quantity will be parsed as a number
				tempCart[i].quantity = parseFloat(value)
				
				//set the new subtotal
				tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
			}
		}

		//set our cart state with the new quantities
		setCart(tempCart)

		//set our localStorage cart as well
		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}


	// Add to Cart function 
	const addToCart = () => {
		//variable to determine if the item we are adding is already in our cart or not
		let alreadyInCart = false
		//variable for the item's index in the cart array, if it already exists there
		let productIndex
		//temporary cart array
		let cart = []

		// Checking local storage 
		if(localStorage.getItem('cart')){
			cart = JSON.parse(localStorage.getItem('cart'))
		}

		//loop through our cart to check if the item we are adding is already in our cart or not
		for(let i = 0; i < cart.length; i++){
			if(cart[i].productId === productId){
				//if it is, make alreadyInCart true
				alreadyInCart = true
				productIndex = i
			}
		}

		//if a product is already in our cart, just increment its quantity and adjust its subtotal
		if(alreadyInCart){
			console.log(quantity)
			
			cart[productIndex].quantity += quantity
			cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity
			Swal.fire({
					title: "Added to cart!",
					icon: 'success',
					text: "You have successfully added this product to your cart.",
					confirmButtonColor: '#FFB138'
			})
		}else{
			//else add a new entry in our cart, with values from states that need to be set wherever this function goes
			cart.push({
				'productId': productId,
				'name': name,
				'price': price,
				'quantity': quantity,
				'subtotal': price * quantity
			})
			Swal.fire({
					title: "Added to cart!",
					icon: 'success',
					text: "You have successfully added this product to your cart.",
					confirmButtonColor: '#FFB138'
			})
		}

		//set our localStorage cart as well
		localStorage.setItem('cart', JSON.stringify(cart))
	}

	
	const increment = () => {
	    setQuantity(prevCount => prevCount+1);
	}

    const decrement = () => {
        if(quantity>1){
        setQuantity(quantity - 1)
        }
    }



	return(
		<Container>

			<Card className="mt-5">
				<Card.Header className="bg-dark text-white text-center pb-0 font">
					<h4>{name}</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text><h4> {description} </h4></Card.Text>
					
					<p> Size: {size} </p>
					<p> PCD: {PCD} </p>
					<p> Offset: {offset} </p>
					<p> Color: {color} </p>
					<p> Quantity(Set of 4):
						<div className="qty mt-2 mb-1 input-group">

							<Button variant="outline-dark" type="button" onClick={() => decrement()}>-</Button>
							
							<input min="1" type="number" value={quantity} onChange={e => setQuantity(e.target.value)}></input>
							
							<Button variant="outline-dark" type="button" onClick={() => increment()}>+</Button>
						
						</div>
					</p>
					<h6> Price: Php {price} </h6>
				</Card.Body>
				<Card.Footer>
					{user.id !== null
						? <Button variant="warning" block onClick={() => addToCart()}>Add to Cart</Button>

						: <Link className='btn btn-danger btn-block' to='/login'>Log in to Order</Link>

					}
				</Card.Footer>
			</Card>

		</Container>
	)
}